# Remain In Place! Subtitles

## Introduction
This project aim to fix most common issues encountered while using subtitles (fixed and linear drifted offsets).

## Library
- C++11 - C++ standard revision 2011

## Compilation
- make all - Deployement compilation
- make asan - Test compilation using the ASan library (Address Sanitizer)

## Options
Usage : ./ripsub

## Code style
C++ 11.  
Code is intended with 4 spaces, no tabs.  
A line should not exceed 72 characters.  
It follows Allman indent style.  
