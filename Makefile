PROJECTNAME = RIPSub (Remain in Place! Subtitles)
HOMEPAGE    = https://gitlab.com/reidy/RIPSub
LICENSE     = GNU GPLv3

CXX      = g++
CXXFLAGS = -Wall -Wextra -std=c++11
LDFLAGS  = -fsanitize=address -fsanitize=undefined -fsanitize=shift -fsanitize=integer-divide-by-zero -fsanitize=unreachable -fsanitize=vla-bound -fsanitize=null -fsanitize=return -fsanitize=signed-integer-overflow -fsanitize=bool -fsanitize=enum
RM       = rm -rf

HEADDIR  = ./src
SRCDIR   = ./src
OBJDIR   = ./obj
BINDIR   = ./

SOURCES  = $(wildcard $(SRCDIR)/*.cpp)
INCLUDES = $(wildcard $(HEADDIR)/*.h)
OBJECTS  = $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
BINARY = $(BINDIR)/ripsrt
ASANBIN = $(BINDIR)/ripsrt-test

all: $(BINARY)
$(BINARY): $(OBJECTS)
	@$(CXX) $^ -o $@
	@echo "Compiled" $@ "successfully."

asan: $(ASANBIN)
$(ASANBIN): $(OBJECTS)
	@$(CXX) $(LIBS) $(LDFLAGS) $^ -o $@
	@echo "Compiled" $@ "successfully with libasan."

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@mkdir -p $(OBJDIR)
	@$(CXX) $(CXXFLAGS) -c $< -o $@
	@echo "(CXX) "$<""

clean:
	@$(RM) $(OBJECTS)
	@echo "Objects removed."

remove: clean
	@$(RM) $(BINDIR)/$(BINARY) $(BINDIR)/$(ASANBIN)
	@echo "Executable removed."

.PHONY: all asan clean remove
