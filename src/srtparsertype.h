/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Remain In Place! Subtitles
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <list>
#include "srttimes.h"

class SRTParserType
{

public:

                            SRTParserType() {}
                            ~SRTParserType() {}

    int                     GetID() { return m_iID; }
    void                    SetID( int iID ) { m_iID = iID; }

    std::string             GetTimes() const { return m_oTimes.toString(); }
    void                    SetTimes( std::string sTime ) { m_oTimes.FillTimes( sTime ); }
    void                    SetTimesOffset( int iOffset ) { m_oTimes.OffsetTimes( iOffset, false ); }

    std::string             GetText() { return m_sText; }
    void                    AddText(std::string sText) { m_sText += sText; }
    void                    Reset() { m_iID = 0; m_oTimes.Reset(); m_sText.clear(); }

    bool                    isComplete() { return ( m_iID && !m_oTimes.isEmpty() && !m_sText.empty() ); }

private:

    SRTParserState::State   m_eState;
    int                     m_iID;
    SRTTimes                m_oTimes;
    std::string             m_sText;
};
