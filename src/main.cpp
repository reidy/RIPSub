/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Remain In Place! Subtitles
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <cstring>
#include <iostream>
#include "srtparser.h"


void DisplayHelp()
{
    std::cout << "Remain In Place Subtitles!"
    << std::endl
    << "Options:" << std::endl
    << "\t[-d]\t\tDebug" << std::endl
    << "\t[-o] time \tOffset miliseconds" << std::endl
    << "\t[-s]\t\tSave" << std::endl
    << "\t[-S] filepath\tSave as" << std::endl
    << std::endl
    << "Example:" << std::endl
    << "\t./ripsub Sintel.srt -o 100 -S Sintel_offset_100ms.srt"
    << std::endl;
}

int main( int argc, char** argv )
{
    if( argc <= 1 )
    {
        std::cout << "Not enough parameters." << std::endl << std::endl;
        
        DisplayHelp();

        return EXIT_FAILURE;
    }

    std::ifstream oFile( argv[1] );

    if( !oFile )
    {
        std::cerr << "Cound not parse \"" << argv[1] << "\"." << std::endl;

        return EXIT_FAILURE;
    }

    SRTParser *pParsedFile = new SRTParser( oFile );
    oFile.close();

    int iCurrentArgument = 2;

    while( iCurrentArgument < argc )
    {
        if ( std::strcmp( argv[iCurrentArgument], "-h" ) == 0 )
        {
            DisplayHelp();
        }
        else if( std::strcmp( argv[iCurrentArgument], "-d" ) == 0 )
        {
            pParsedFile->Display();
        }
        else if( std::strcmp( argv[iCurrentArgument], "-o" ) == 0 )
        {
            ++iCurrentArgument;
            if( iCurrentArgument < argc )
            {
                pParsedFile->Offset( std::stoi ( argv[ iCurrentArgument ] ) );
            }
        }
        else if( std::strcmp( argv[iCurrentArgument], "-s" ) == 0 )
        {
            ++iCurrentArgument;
            if( iCurrentArgument < argc )
            {
                pParsedFile->SaveAs( argv[ iCurrentArgument ] );
            }
        }
        else if( std::strcmp( argv[iCurrentArgument], "-S" ) == 0 )
        {
            pParsedFile->SaveAs( argv[ 1 ] );
        }
        else
        {
            std::cerr << "Not recognized symbole: \""
                << argv[iCurrentArgument] << "\"" << std::endl;
        }

        ++iCurrentArgument;
    }

    delete pParsedFile;

    return EXIT_SUCCESS;
}
