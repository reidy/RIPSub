/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Remain In Place! Subtitles
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srttimetype.h"
#include <iostream>
#include <iomanip>
#include <sstream>

SRTTimeType::SRTTimeType( int iTime )
{
    int iRemaining = iTime;

    m_iHH = iTime / 3600000;
    m_iMM = (iRemaining %= 3600000) / 60000;
    m_iSS = (iRemaining %= 60000) / 1000;
    m_iMIL = iRemaining %= 1000;
}

std::string SRTTimeType::toString() const
{
    std::stringstream sTimes;

    sTimes << std::setw(2) << std::setfill('0') << m_iHH
        << ":"
        << std::setw(2) << std::setfill('0') << m_iMM
        << ":"
        << std::setw(2) << std::setfill('0') << m_iSS
        << ","
        << std::setw(3) << std::setfill('0') << m_iMIL;

    return sTimes.str();
}

void SRTTimeType::PrintTimeFormat()
{
    std::cout << toString() << std::endl;
}

void SRTTimeType::Offset( int iOffset )
{
    int iRemaining = iOffset;

    m_iHH += iOffset / 3600000;
    m_iMM += (iRemaining %= 3600000) / 60000;
    m_iSS += (iRemaining %= 60000) / 1000;
    m_iMIL += iRemaining %= 1000;
}
