/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Remain In Place! Subtitles
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srtparser.h"

using namespace SRTParserState;

SRTParser::SRTParser( std::ifstream &oSrtFile )
{
    m_eState = S_ID;
    m_oCurrent.Reset();

    Parse( oSrtFile );
}

void SRTParser::Display()
{
    for ( std::vector< SRTParserType >::iterator itBlockVector = m_vBlockVector.begin(); itBlockVector != m_vBlockVector.end(); ++itBlockVector )
    {
        std::cout
            << "ID :\t" << (*itBlockVector).GetID()
            << std::endl
            << "Time :\t" << (*itBlockVector).GetTimes()
            << std::endl
            << "Text :" << std::endl << (*itBlockVector).GetText()
            << std::endl;
    }
}

void SRTParser::Parse( std::ifstream &oSrtFile )
{
    std::string sLine;

    while ( std::getline( oSrtFile, sLine ) )
    {
        if ( sLine.length() == 0 )
        {
            ParseEmptyLine();
            continue;
        }

        switch ( m_eState )
        {
            case S_ID:
                ParseId( sLine );
                break;
            case S_TIME:
                ParseTimes( sLine );
                break;
            case S_TEXT:
                ParseText( sLine );
                break;
            default:
                ParseError( sLine );
                break;
        }
    }
}

void SRTParser::Offset( int iOffsetTime )
{
    for ( std::vector< SRTParserType >::iterator itBlockVector = m_vBlockVector.begin(); itBlockVector != m_vBlockVector.end(); ++itBlockVector )
    {
        (*itBlockVector).SetTimesOffset( iOffsetTime );
    }
}

void SRTParser::SaveAs( const std::string& sFilePath )
{
    std::ofstream oFile( sFilePath );

    if ( oFile )
    {
        for ( std::vector< SRTParserType >::iterator itBlockVector = m_vBlockVector.begin(); itBlockVector != m_vBlockVector.end(); ++itBlockVector )
        {
            oFile
                << "ID :\t" << (*itBlockVector).GetID()
                << std::endl
                << "Time :\t" << (*itBlockVector).GetTimes()
                << std::endl
                << "Text :" << std::endl << (*itBlockVector).GetText()
                << std::endl;
        }

        std::cout << "File saved as: \"" << sFilePath << "\"." << std::endl;
    }
    else
    {
        std::cout << "File could not be saved as: \"" << sFilePath << "\"." << std::endl;
    }
}

void SRTParser::ParseEmptyLine()
{
    m_eState = S_ID;

    if( m_oCurrent.isComplete() )
    {
        m_vBlockVector.push_back( m_oCurrent );
    }
    m_oCurrent.Reset();
}

void SRTParser::ParseId( std::string &sLine )
{
    m_oCurrent.SetID( std::stoi( sLine ) );
    m_eState = S_TIME;
}

void SRTParser::ParseTimes( std::string &sLine )
{
    m_oCurrent.SetTimes( sLine );
    m_eState = S_TEXT;
}

void SRTParser::ParseText( std::string &sLine )
{
    m_oCurrent.AddText( sLine );
    m_oCurrent.AddText( "\n" );
}

void SRTParser::ParseError( std::string &sLine )
{
    m_eState = S_ERROR;

    std::cerr << "Error: Parsing error at ID "
              << m_vBlockVector.back().GetID() << std::endl
              << sLine << std::endl;
}
