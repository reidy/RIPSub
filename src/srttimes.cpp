/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Remain In Place! Subtitles
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "srttimes.h"
#include <sstream>
#include <vector>

void SRTTimes::FillTimes( std::string& sTime )
{
    std::stringstream oStream( sTime );
    std::string sItem;

    std::getline( oStream, sItem, ':' );
    m_iStartTime += std::stoi( sItem ) * 3600000;
    std::getline( oStream, sItem, ':' );
    m_iStartTime += std::stoi( sItem ) * 60000;
    std::getline( oStream, sItem, ',' );
    m_iStartTime += std::stoi( sItem ) * 1000;
    std::getline( oStream, sItem, ' ' );
    m_iStartTime += std::stoi( sItem );

    std::getline( oStream, sItem, ' ' );

    std::getline( oStream, sItem, ':' );
    m_iEndTime += std::stoi( sItem ) * 3600000;
    std::getline( oStream, sItem, ':' );
    m_iEndTime += std::stoi( sItem ) * 60000;
    std::getline( oStream, sItem, ',' );
    m_iEndTime += std::stoi( sItem ) * 1000;
    std::getline( oStream, sItem );
    m_iEndTime += std::stoi( sItem );
}

std::string SRTTimes::toString() const
{
    std::ostringstream time;
    SRTTimeType s(m_iStartTime), e(m_iEndTime);

    time << std::setw(2) << std::setfill('0') << std::internal;
    time << s.toString() << " --> " << e.toString();

    return time.str();
}

bool SRTTimes::isEmpty()
{
    return (m_iEndTime == 0);
}

void SRTTimes::Reset()
{
    m_iStartTime = 0;
    m_iEndTime = 0;
}

void SRTTimes::OffsetTimes( int iOffsetTime, bool bLinearCorrection = false )
{
    if ( !bLinearCorrection )
    {
        m_iStartTime += iOffsetTime;
        m_iEndTime += iOffsetTime;
    }
}
