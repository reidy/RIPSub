/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Remain In Place! Subtitles
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include "srtparserstate.h"
#include "srtparsertype.h"

class SRTParser
{

public:

                                    SRTParser       ( std::ifstream& oSrtFile );
    void                            Display         ();
    void                            Offset          ( int iOffsetTime );
    void                            SaveAs          ( const std::string& sFilePath );
private:    
    
    void                            Parse           ( std::ifstream& oSrtFile );
    void                            ParseEmptyLine  ();
    void                            ParseId         ( std::string& sLine );
    void                            ParseTimes      ( std::string& sLine );
    void                            ParseText       ( std::string& sLine );
    void                            ParseError      ( std::string& sLine );

    std::vector< SRTParserType >    m_vBlockVector;
    SRTParserType                   m_oCurrent;
    SRTParserState::State           m_eState;

};
